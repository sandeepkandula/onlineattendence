import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacultyRoutingModule } from './faculty-routing.module';
import { DataentryComponent } from './dataentry/dataentry.component';
import { DatashowComponent } from './datashow/datashow.component';
import { ViewComponent } from './view/view.component';


@NgModule({
  declarations: [DataentryComponent, DatashowComponent, ViewComponent],
  imports: [
    CommonModule,
    FacultyRoutingModule
  ]
})
export class FacultyModule { }
