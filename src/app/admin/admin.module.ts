import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { BatchdetailsComponent } from './batchdetails/batchdetails.component';
import { StudentDetailsComponent } from './student-details/student-details.component';
import { IndStudenttrackingComponent } from './ind-studenttracking/ind-studenttracking.component';


@NgModule({
  declarations: [AdminHomeComponent, BatchdetailsComponent, StudentDetailsComponent, IndStudenttrackingComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
