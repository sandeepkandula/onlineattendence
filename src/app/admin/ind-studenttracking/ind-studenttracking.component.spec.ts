import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndStudenttrackingComponent } from './ind-studenttracking.component';

describe('IndStudenttrackingComponent', () => {
  let component: IndStudenttrackingComponent;
  let fixture: ComponentFixture<IndStudenttrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndStudenttrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndStudenttrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
